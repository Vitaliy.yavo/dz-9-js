/*Відповідь 1:
1)Використавши метод document.createElement('tag-name'),де 'tag-name' - ім'я нового тега;
2)Отримати батківський елемент, до якого додати новий тег: наприклад через document.getElementById();
3)Додати новий тег як дочірній елемент до батьківського елемента, за допомогою методу appendChild().*/

/*Відповідь 2:
insertAdjacentHTML - це метод, який доступний для елементів DOM і використовується для вставки HTML-коду або розмітки вказаним способом поруч з елементом.
___Синтаксис: element.insertAdjacentHTML(position, text):
1)element: Елемент DOM, до якого буде застосована операція вставки HTML-коду.
2)position: Перший параметр, який вказує місце, де буде вставлений HTML-код. 
Можливі значення цього параметра:
___"beforebegin": Вставляє HTML-код перед елементом (як сусідний попередній елемент).
___"afterbegin": Вставляє HTML-код в сам початок внутрішнього вмісту елемента (перед першим дочірнім елементом).
___"beforeend": Вставляє HTML-код в сам кінець внутрішнього вмісту елемента (після останнього дочірнього елемента).
___"afterend": Вставляє HTML-код після елемента (як сусідний наступний елемент).
3)text: Рядок, який містить HTML-код або розмітку, який буде вставлений вказаним способом.*/

/*Відповідь 3:
Є два способи як видалити елементи зі сторінки:
1)Видалення елемента з його батьківського елемента: використовуючи метод remove() для елемента, який треба видалити.
Наприклад: element.remove();

2)Видалення дочірнього елемента з батьківського елемента: отримати батьківський елемент і використваши метод removeChild(),
передавши в якості аргументу дочірній елемент, який треба видалити. 
Наприклад: parentElement.removeChild(childElement);
*/

// Задача:

const createListFromArray = (array, parent = document.body) => {
  const listElement = document.createElement("ul");

  array.forEach((item) => {
    const listItem = document.createElement("li");
    listItem.textContent = item;
    listElement.appendChild(listItem);
  });

  parent.appendChild(listElement);
};

const exampleArray1 = ["hello", "world", "Kiev", "Kharkiv", "Odessa", "Lviv"];
const exampleArray2 = ["1", "2", "3", "sea", "user", 23];

createListFromArray(exampleArray1);
createListFromArray(exampleArray2, document.getElementById("listContainer"));
